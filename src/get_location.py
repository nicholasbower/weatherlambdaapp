import json
# from botocore.vendored import requests
import requests

def lambda_handler(event, context):
    ip = event
    # ip = event['queryStringParameters']['ip']
    r = requests.get(f'https://ipvigilante.com/{ip}', verify=False)
    if r.status_code != 200:
        return {
            "statusCode": 500,
            "body": "Invalid Input"
        }
    data = r.json()
    lat = data.get('data').get('latitude')
    lon = data['data']['longitude']
    city = data['data']['city_name']

    
    
    res = { "lat": lat, "lon": lon, "city": city }
    return {
        'statusCode': 200,
        'body': json.dumps(res)
    }
