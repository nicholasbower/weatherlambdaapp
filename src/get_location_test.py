import unittest
from get_location import lambda_handler


class TestGetLocation(unittest.TestCase):
    def test_get_cords(self):
        event = {
            "queryStringParameters": 
                {
                    "ip": '8.8.8.8'
                }
            }
        res = lambda_handler(event['queryStringParameters']['ip'], {})
        
        self.assertEqual(res['statusCode'], 200)

    
    def test_get_cords_failure(self):
        event = {
            "queryStringParameters": {
                "ip": "This should fail..."
            }
        }

        res = lambda_handler(event["queryStringParameters"]["ip"], {})
        self.assertEqual(res["statusCode"], 500)


if __name__ == '__main__':
    unittest.main()